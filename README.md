SUMMARY
-------
This module brings [EdStep](https://edstep.com/) to your Drupal site. It allows you to promote your EdStep courses and users can take them without leaving your website. This way you are in full control over the EdStep course experience.


REQUIREMENTS
------------
* PHP 5.6 or higher
* An EdStep account. Sign up for one at https://edstep.com/
* An EdStep client ID. You can apply for one at https://edstep.com/


INSTALLATION
------------
1. Install the module.
2. Install dependencies via Composer.


CONFIGURATION
-------------
1. Go to admin/config/system/edstep and enter your client ID and secret.
2. Go to admin/people/permissions and decide which roles may add, view and remove EdStep courses.


CONTACT
-------
Current maintainers:
* Daniel Hjellström [(yellstorm)](http://drupal.org/user/3550376)
* Fredrik Johansson [(jeanfredrik)](http://drupal.org/user/835154)
* Emil Lingsell [(emil.lingsell)](http://drupal.org/user/1592606)
* Jonas Seffel [(jseffel)](http://drupal.org/user/350272)
* Johan Westin [(johan_westin)](http://drupal.org/user/2314400)

This project has been sponsored by:
* Cerpus Sverige AB
  Specialized in consulting and planning of Drupal powered sites. Offering
  installation, development, theming, customization, and hosting to get you
  started. Visit https://cerpus.se for more information.

* Cerpus AS
  Creators of the SaaS plattform [EdStep.com](https://edstep.com). Visit https://cerpus.com/ for more information.
