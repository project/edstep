<?php

namespace Drupal\edstep;

use Drupal\Core\Entity\EntityViewBuilder;

/**
 * View builder handler for edstep_courses.
 */
class EdstepCourseViewBuilder extends EntityViewBuilder {

}
